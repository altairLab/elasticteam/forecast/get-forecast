#!/bin/bash

BLACK="\033[0;30m"
RED="\033[0;31m"
GREEN="\033[0;32m"
LGREEN="\033[1;32m"
YELLOW="\033[1;33m"
NC="\033[0m"

should_reboot=false

confirm() {
while true; do
    read -p $1 yn
    case $yn in
        [Yy]* ) return 0;;
        [Nn]* ) return 1;;
        * ) echo "Please answer yes or no.";;
    esac
done

}

if [[ $EUID -eq 0 ]]
then
   echo -e "This script must ${RED}NOT${NC} be run with root privileges!"
   exit 1
fi

if groups $USER | grep -q "\bdialout\b"
then
    echo -e "${GREEN}User '$USER' is already a member of group dialout.${NC}"
else
    echo -e "${YELLOW}User '$USER' is not a member of group dialout.${NC}"
    echo "The dialout group grants full and direct access to serial ports."
    echo "Remember to restart the PC after the installation process ends!"
    sudo adduser $USER dialout
    should_reboot=true
fi

function install_gui() {
    if [[ $(command -v python3) ]]; then
        echo -e "${GREEN}Python3 is already installed!${NC}"
    else
        echo -e "${RED}Python3 is not installed.${NC}"
        echo -e "${YELLOW}Install a version of Python3 >= 3.6${NC}"
        exit 1
    fi

    if [[ $(command -v pip3) ]]; then
        echo -e "${GREEN}Pip (for Python3) is already installed!${NC}"
    else
        echo -e "${RED}Pip (for Python3) is not installed.${NC}"
        echo -e "${YELLOW}Install it using your package manager 'sudo apt python3-pip' and re-run the script${NC}"
        exit 1
    fi

    echo "Updating pip and setuptools"
    pip3 install --upgrade pip setuptools


    printf "\nSelect which version of the UI to install.\n"

    function install_qt() {
        sudo apt update
        sudo apt install --reinstall libxcb-xinerama0
        if [[ $(dpkg -l | grep libxcb-xinerama0) ]]; then
            printf "\n${GREEN}Shared libraries installed!${NC}\n"
        else
            printf "${RED}\nCould not install package 'libxcb-xinerama0'.\nTry to install the UI manually.\n${NC}"
            exit 1
        fi
        pip3 install --no-cache-dir --force-reinstall forecastui[qt]
    }

    function install_minimal() {
        pip3 install --no-cache-dir --force-reinstall forecastui
    }

    PS3="Please enter your choice: "
    options=("Install with Qt wrappers (recommended)" "Minimal install" "Quit")
    select opt in "${options[@]}"
    do
        case $opt in
            "Install with Qt wrappers (recommended)")
                install_qt
                break;
                ;;
            "Minimal install")
                install_minimal
                break;
                ;;
            "Quit")
                break;
                ;;
            *) echo "Invalid option $REPLY";;
        esac
    done

    if [[ $(pip3 freeze | grep forecastui) ]]; then
        printf "\n${GREEN}ForecastUI has been installed!${NC}\n"
    else
        printf "\n${RED}ForecastUI was not installed.${NC}\n"
    fi
}

function install_board() {
    if [[ $(command -v git) ]]; then
        echo -e "${GREEN}Git is already installed!${NC}"
    else
        echo -e "${YELLOW}Need to install git in order to download the board software${NC}"
        sudo apt install git
    fi
    git clone --recurse-submodules git@gitlab.com:altairLab/elasticteam/ForecastNucleoFramework-test.git
}

function install_sesim() {
	if [[ $(command -v matlab) ]]; then
		echo -e "${GREEN}Matlab is already installed!${NC}"
	else
		echo -e "${RED}Matlab is not installed.${NC}"
		echo -e "${YELLOW}Install Matlab with Simulink.${NC}"
		exit 1
	fi
    sudo apt update
    cd ~/forecast
    git clone git@gitlab.com:altairLab/elasticteam/SESim.git
}

gui_flag=false
board_flag=false
sesim_flag=false
printf "Install the user interface? "
if confirm "[Y/n]"; then gui_flag=true; fi
printf "Git clone the board software? "
if confirm "[Y/n]"; then board_flag=true; fi
printf "Git clone the simulation and evaluation software? "
if confirm "[Y/n]"; then sesim_flag=true; fi

if [ "$gui_flag" = true ]
then
    install_gui
fi

if [ "$board_flag" = true ]
then
    install_board
fi

if [ "$sesim_flag" = true ]
then
    install_sesim
fi

if [ "$should_reboot" = true ]; then
    printf "${YELLOW}\n\nRemember to reboot!\n\n${NC}"
fi
